#!/usr/bin/evn python
# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   文件名称 :     export_file_helper
   文件功能描述 :   功能描述
   创建人 :       小钟同学
   创建时间 :          2021/4/20
-------------------------------------------------
   修改描述-2021/4/20:         
-------------------------------------------------
"""
# from flask import request,make_response,send_file


class File:

    def export_zip_files(self, dirpath):
        """查询导出文件"""
        import os
        import zipfile
        from io import BytesIO
        try:
            memory_file = BytesIO()
            dsplit = dirpath.split('\\')
            dname = None
            if len(dsplit) >= 2:
                dname = dsplit[-2]
            with zipfile.ZipFile(memory_file, "w", zipfile.ZIP_DEFLATED) as zf:
                for path, dirnames, filenames in os.walk(dirpath):
                    if dname:
                        hr = path.split(dname, 1)
                        for filename in filenames:
                            zf.write(os.path.join(path, filename), os.path.join(*hr[1].split('\\'), filename))
                    else:
                        for filename in filenames:
                            zf.write(os.path.join(path, filename))
                # zf.setpassword("kk-123456".encode('utf-8'))
            memory_file.seek(0)
            return memory_file, None
        except Exception as e:
            return None, str(e)


