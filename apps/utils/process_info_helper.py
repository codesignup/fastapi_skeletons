#!/usr/bin/evn python
# coding=utf-8


import psutil
import os
import datetime


def info():
    pid = os.getpid()
    p = psutil.Process(pid)
    print('----------------')
    # 进程ID
    print('Process id : %d' % pid)
    # python-daemon是一个Python的第三方库。
    # 其核心功能是创建一个Daemon进程（后台运行以及脱离当前父进程管控）
    # 本文中，我们将会基于如上两个场景进行分析。

    # psutil.process_iter() 方法可以返回进程列表信息，再通过匹配名称，获取进程的 pid 即可。
    # p.username()
    # username
    print('Process username : %s' % p.username())
    # 进程NAME
    print('Process name : %s' % p.name())
    # 获取进程bin路径
    print('Process bin  path : %s' % p.exe())
    # 获取pid对应的路径
    print('Process path : %s' % p.cwd())
    # 进程状态
    print('Process status : %s' % p.status())
    # 进程运行时间
    print('Process creation time : %s' % datetime.datetime.fromtimestamp(p.create_time()).strftime("%Y-%m-%d %H:%M:%S"))
    # CPU使用情况
    print(p.cpu_times())
    # 内存使用情况
    print('Memory usage : %s%%' % p.memory_percent())
    # 硬盘读取信息
    print(p.io_counters())
    # 打开进程socket的namedutples列表
    print(p.connections())
    # 此进程的线程数
