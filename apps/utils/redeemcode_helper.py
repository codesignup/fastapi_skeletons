#!/usr/bin/evn python
# coding=utf-8
# + + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + +
#        ┏┓　　　┏┓+ +
# 　　　┏┛┻━━━┛┻┓ + +
# 　　　┃　　　　　　 ┃ 　
# 　　　┃　　　━　　　┃ ++ + + +
# 　　 ████━████ ┃+
# 　　　┃　　　　　　 ┃ +
# 　　　┃　　　┻　　　┃
# 　　　┃　　　　　　 ┃ + +
# 　　　┗━┓　　　┏━┛
# 　　　　　┃　　　┃　　　　　　　　　　　
# 　　　　　┃　　　┃ + + + +
# 　　　　　┃　　　┃　　　　Codes are far away from bugs with the animal protecting　　　
# 　　　　　┃　　　┃ + 　　　　神兽保佑,代码无bug　　
# 　　　　　┃　　　┃
# 　　　　　┃　　　┃　　+　　　　　　　　　
# 　　　　　┃　 　　┗━━━┓ + +
# 　　　　　┃ 　　　　　　　┣┓
# 　　　　　┃ 　　　　　　　┏┛
# 　　　　　┗┓┓┏━┳┓┏┛ + + + +
# 　　　　　　┃┫┫　┃┫┫
# 　　　　　　┗┻┛　┗┻┛+ + + +
# + + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + ++ + + +"""
"""
Author = zyx
@Create_Time: 2019/4/29 14:48
@version: v1.0.0
@Contact: 308711822@qq.com
@File: redeemcode_helper.py
@文件功能描述:
"""

import random  # 引入随机数模块，Python自带，无需安装


# 定义生成优惠券编号的函数，num：是要生成的个数，len：生成优惠券的长度
def creat_discount(tag='', num=2, len=2):
    str = 'uiuiyfgdffgqwertyuiopasdfghjklzxcvbnm1234567890'  # 优惠券会用到的字符
    discount_list = []  # 优惠券存在这个数组中
    for i in range(num):
        a = ''
        for j in range(len):  # 每次循环生成一个优惠券
            a += random.choice(str)
        discount_list.append(tag.upper() + a.upper())  # 生成好的优惠券放入到数组中
    return discount_list


# sdsd=['a','a','ss','df','a']

if __name__ == '__main__':
    kkkk = creat_discount(tag='QY01', num=40000, len=12)
    print(len(kkkk))
    kkkk = list(set(kkkk))
    print(len(kkkk))
    with open('xtest.txt', 'w') as f:
        for iii in kkkk:
            f.writelines(iii+'\n')
