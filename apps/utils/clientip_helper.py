#!/usr/bin/evn python
# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   文件名称 :     clientip_helper
   文件功能描述 :   功能描述
   创建人 :       小钟同学
   创建时间 :          2021/9/27
-------------------------------------------------
   修改描述-2021/9/27:         
-------------------------------------------------
"""
from starlette.requests import Request

def get_client_ip(request: Request):
    """
    获取客户端真实ip
    :param request:
    :return:
    """
    forwarded = request.headers.get("X-Forwarded-For")
    if forwarded:
        return forwarded.split(",")[0]
    return request.client.host