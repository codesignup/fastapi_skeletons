#!/usr/bin/evn python
# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   文件名称 :     ip
   文件功能描述 :   功能描述
   创建人 :       小钟同学
   创建时间 :          2021/9/27
-------------------------------------------------
   修改描述-2021/9/27:         
-------------------------------------------------
"""
from fastapi.security import HTTPBearer, HTTPAuthorizationCredentials
from typing import Optional
from starlette.requests import Request
from apps.utils.clientip_helper import get_client_ip
from apps.response.json_response import Fail

# 使用依赖注入的方式来检测获取用户的请求IP
def client_ip_checker(request: Request):
    return get_client_ip(request)